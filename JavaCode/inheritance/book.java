package inheritance;

public class book {
    protected String title;
    private String author;

    public String getTitle (){
        return this.title;
    }

    public String getAuthor (){
        return this.author;
    }

    public book(String title, String author) {
        this.title = title;
        this.author = author;
    }
    public String toString(){
        return "The book " + this.title + " was written by " + this.author;
    }
}


