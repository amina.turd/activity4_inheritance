package inheritance;

public class ElectronicBook extends book{

    private double numberBytes;

    public double getNumberBytes (){
        return this.numberBytes;
    }
    
    public ElectronicBook(String title, String author, double numberBytes){
        super(title, author);
        this.numberBytes = numberBytes;
    }

    public String toString() {
        String fromBase = super.toString();
        fromBase = fromBase + " the nuber of bytes it takes is " + this.numberBytes;
        return fromBase; 
    } 
}
