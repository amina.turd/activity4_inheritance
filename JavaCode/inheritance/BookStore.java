package inheritance;

public class BookStore {
    public static void main(String[] args){
        book[] b = new book[5];
        b[0] = new book("Book 1", "A");
        b[1] = new ElectronicBook("Book 2", "B", 10);
        b[2] = new book("Book 3", "C");
        b[3] = new ElectronicBook("Book 4", "D", 15);
        b[4] = new ElectronicBook("Book 5", "E", 20);

        for (int i=0; i<b.length ; i++){
            if (b[i] instanceof ElectronicBook){
                System.out.println((ElectronicBook)b[i]);
             }
              else{
                System.out.println(b[i]);
             }
        }

    }
}
